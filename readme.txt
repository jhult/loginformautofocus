﻿*******************************************************************************
** Component Information
*******************************************************************************

	Component:              LoginFormAutofocus
	Author:                 Jonathan Hult
	Website:                http://jonathanhult.com
	Last Updated On:        build_1_20130612

*******************************************************************************
** Overview
*******************************************************************************

	This component sets the username field on the login page to be 
	autofocused when the page loads.
	
	Templates:
		LOGIN_PAGE - Overrides the original and simply calls the dynamichtml 
		include login_page.
	
	Dynamichtml includes:
		login_page - Includes original login_page.htm template plus 2 edits 
		to make the username field autofocus.
	
*******************************************************************************
** COMPATIBILITY WARNING
*******************************************************************************

	This component was built upon and tested on the version listed below, 
	this is the only version it is "known" to work on, but it may well work 
	on older/newer versions.
	- 11gR1-11.1.1.7.0-idcprod1-130304T092605 (Build: 7.3.4.184)

*******************************************************************************
** HISTORY
*******************************************************************************
	
	build_1_20130612
		- Initial component release
